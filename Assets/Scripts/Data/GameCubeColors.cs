using UnityEngine;

namespace DefaultNamespace.Data
{
    [CreateAssetMenu(fileName = "Game cube colors", menuName = "Game cube colors", order = 0)]
    public class GameCubeColors : ScriptableObject
    {
        [SerializeField] private Material[] _colors;

        public Material GetMaterial(int gameCubeLevel)
        {
            while (gameCubeLevel >= _colors.Length)
            {
                gameCubeLevel -= _colors.Length;
            }

            return _colors[gameCubeLevel];
        }
    }
}