
namespace DefaultNamespace
{
    public class GameVibration
    {
        private bool _vibration = true;
        public bool Vibration { get => _vibration; set => _vibration = value; }
        
        public void AddCubeForTracking(GameCube gameCube)
        {
            gameCube.LevelIsUp += SingleHeavyVibrate;
            gameCube.RunCube += SingleVibrate;
        }

        public void RemoveCubeForTracking(GameCube gameCube)
        {
            gameCube.LevelIsUp -= SingleHeavyVibrate;
            gameCube.RunCube -= SingleVibrate;
        }

        public void SingleVibrate()
        {
            if (Vibration)
            {
                Utilities.Vibration.Vibrate(25,255,true);
            }
        }

        private void SingleHeavyVibrate(int level)
        {
            if (Vibration)
            {
                Utilities.Vibration.Vibrate(75, 255, true);
            }
        }
    }
}