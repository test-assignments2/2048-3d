using DefaultNamespace;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    internal class GameOverPanel : MonoBehaviour
    {
        [SerializeField] private Button _newGameButton;

        private GameManager _gameManager;

        public void Init(GameManager gameManager)
        {
            _gameManager = gameManager;
            _newGameButton.onClick.AddListener(OnNewGameButtonClick);
        }

        private void OnNewGameButtonClick()
        {
            _gameManager.StartGame();
            gameObject.SetActive(false);
        }

        private void OnDestroy()
        {
            _newGameButton.onClick.RemoveListener(OnNewGameButtonClick);
        }
    }
}