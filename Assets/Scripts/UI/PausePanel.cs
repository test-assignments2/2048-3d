using DefaultNamespace;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    internal class PausePanel : MonoBehaviour
    {
        [SerializeField] private Button _restartGameButton;
        [SerializeField] private Button _exitButton;
        [SerializeField] private Toggle _musicToggle;
        [SerializeField] private Toggle _soundToggle;
        [SerializeField] private Toggle _vibrationToggle;
        
        private GameManager _gameManager;
        private GameSounds _gameSounds;
        private GameVibration _gameVibration;

        public void Init(GameManager gameManager, GameSounds gameSounds, GameVibration gameVibration)
        {
            _gameVibration = gameVibration;
            _gameManager = gameManager;
            _gameSounds = gameSounds;
            _restartGameButton.onClick.AddListener(OnRestartGameButtonClick);
            _exitButton.onClick.AddListener(OnExitButtonClick);
            _musicToggle.onValueChanged.AddListener(OnMusicToggleChange);
            _soundToggle.onValueChanged.AddListener(OnSoundToggleChange);
            _vibrationToggle.onValueChanged.AddListener(OnVibrationToggleChange);
        }

        private void OnVibrationToggleChange(bool value)
        {
            _gameVibration.Vibration = value;
        }

        private void OnSoundToggleChange(bool value)
        {
            _gameSounds.Sounds = value;
        }

        private void OnMusicToggleChange(bool value)
        {
            _gameSounds.Music = value;
        }

        private void OnExitButtonClick()
        {
            gameObject.SetActive(false);
            _gameManager.DisablePause();
        }

        private void OnRestartGameButtonClick()
        {
            _gameManager.DisablePause();
            _gameManager.StartGame();
            gameObject.SetActive(false);
        }

        private void OnDestroy()
        {
            _restartGameButton.onClick.RemoveListener(OnRestartGameButtonClick);
            _exitButton.onClick.RemoveListener(OnExitButtonClick);
            _musicToggle.onValueChanged.RemoveListener(OnMusicToggleChange);
            _soundToggle.onValueChanged.RemoveListener(OnSoundToggleChange);
            _vibrationToggle.onValueChanged.RemoveListener(OnVibrationToggleChange);
        }
    }
}