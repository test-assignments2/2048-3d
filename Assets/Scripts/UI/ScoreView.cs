using TMPro;
using UnityEngine;

namespace UI
{
    public class ScoreView : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _text;

        public void SetScore(int score)
        {
            _text.text = score.ToString();
        }
    }
}