using System;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Input
{
    public class TouchscreenGameplayInput
    {
        public event Action<Vector2> MoveInputReceived;
        public event Action TouchInputReleaseReceived;

        private readonly InputMap _inputMap;
        private readonly RectTransform _inputMoveZone;

        private bool _canReleaseReceive = false;

        public TouchscreenGameplayInput(InputMap inputMap, RectTransform inputMoveZone)
        {
            _inputMap = inputMap;
            _inputMoveZone = inputMoveZone;
            
            _inputMap.Touchscreen.TouchPress.started += OnTouchPressStarted;
            _inputMap.Touchscreen.TouchPress.canceled += OnTouchPressCanceled;
        }

        private void OnTouchPressStarted(InputAction.CallbackContext context)
        {
            var isTouchInRect = CheckTouchInRect();

            if (isTouchInRect)
            {
                _inputMap.Touchscreen.TouchDelta.performed += OnTouchDeltaPerformed;
                _canReleaseReceive = true;
            }
        }

        private void OnTouchPressCanceled(InputAction.CallbackContext context)
        {
            _inputMap.Touchscreen.TouchDelta.performed -= OnTouchDeltaPerformed;
            var isTouchInRect = CheckTouchInRect();

            if (_canReleaseReceive && isTouchInRect)
            {
                TouchInputReleaseReceived?.Invoke();
            }

            _canReleaseReceive = false;
        }

        private void OnTouchDeltaPerformed(InputAction.CallbackContext context)
        {
            MoveInputReceived?.Invoke(context.ReadValue<Vector2>());
        }

        private bool CheckTouchInRect()
        {
            var currentTouchPosition = _inputMap.Touchscreen.TouchPosition.ReadValue<Vector2>();
            return RectTransformUtility.RectangleContainsScreenPoint(_inputMoveZone, currentTouchPosition);
        }
    }
}