using DefaultNamespace;
using UnityEngine;

namespace Input
{
    public class GameCubeRunHandler : MonoBehaviour
    {
        [SerializeField] private CubeLauncher _cubeLauncher;
        private GameplayInputManager _inputManager;

        public void Init(GameplayInputManager inputManager)
        {
            _inputManager = inputManager;
        }

        private void Start()
        {
            _inputManager.ReleaseInputReceived += OnReleaseInputReceived;
        }

        private void OnReleaseInputReceived()
        {
            _cubeLauncher.RunCube();
        }
    }
}