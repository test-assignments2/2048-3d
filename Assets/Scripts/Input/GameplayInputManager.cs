using System;
using UnityEngine;

namespace Input
{
    public class GameplayInputManager : MonoBehaviour
    {
        public event Action<Vector2> MoveInputReceived;
        public event Action ReleaseInputReceived;

        private RectTransform _moveInputZone;
        private InputMap _inputMap;
        private TouchscreenGameplayInput _touchscreenInput;

        public void Init(RectTransform moveInputZone)
        {
            _moveInputZone = moveInputZone;
            _inputMap = new InputMap();
            _inputMap.Enable();
            InitTouchscreenInput(_inputMap);
        }

        public void Enable()
        {
            _inputMap.Enable();
        }

        public void Disable()
        {
            _inputMap.Disable();
        }

        private void InitTouchscreenInput(InputMap inputMap)
        {
            _touchscreenInput = new TouchscreenGameplayInput(inputMap, _moveInputZone);
            
            _touchscreenInput.MoveInputReceived += OnMoveInputReceived;
            _touchscreenInput.TouchInputReleaseReceived += OnReleaseInputReceived;
        }

        private void OnReleaseInputReceived()
        {
            ReleaseInputReceived?.Invoke();
        }

        private void OnMoveInputReceived(Vector2 delta)
        {
            MoveInputReceived?.Invoke(delta);
        }
    }
}