using DefaultNamespace;
using UnityEngine;

namespace Input
{
    public class CubeLauncherMoveHandler : MonoBehaviour
    {
        [SerializeField] private StartPosition _startPosition;
        [SerializeField] private CubeLauncher _cubeLauncher;
        [SerializeField] public float _sensitivity = 1f;
        
        private GameplayInputManager _inputManager;
        private float _xPosition = 0f;

        public void Init(GameplayInputManager inputManager)
        {
            _inputManager = inputManager;
            _cubeLauncher.transform.position = new Vector3(0, _startPosition.Y, _startPosition.Z);
        }

        private void Start()
        {
            _inputManager.MoveInputReceived += OnMoveInputReceived;
        }

        private void OnDestroy()
        {
            _inputManager.MoveInputReceived -= OnMoveInputReceived;
        }

        private void OnMoveInputReceived(Vector2 delta)
        {
            _xPosition += _sensitivity * delta.x * Time.deltaTime;
            _xPosition = Mathf.Clamp(_xPosition, _startPosition.LeftX, _startPosition.RightX);

            _cubeLauncher.transform.position = new Vector3(_xPosition, _startPosition.Y, _startPosition.Z);
        }
    }
}