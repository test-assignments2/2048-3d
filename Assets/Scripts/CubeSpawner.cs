using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Pool;

namespace DefaultNamespace
{
    public class CubeSpawner : MonoBehaviour
    {
        [SerializeField] private GameCube _gameCubePrefab;
        
        private ObjectPool<GameCube> _gameCubePool;
        private int _serialNumberCounter;
        private ScoreCounter _scoreCounter;
        private List<GameCube> _allGameCubes = new List<GameCube>();
        private GameSounds _gameSounds;
        private GameVibration _gameVibration;

        public void Init(ScoreCounter scoreCounter, GameSounds gameSounds, GameVibration gameVibration)
        {
            _gameVibration = gameVibration;
            _gameSounds = gameSounds;
            _scoreCounter = scoreCounter;
            CreateGameCubePool();
        }

        public GameCube CreateGameCube()
        {
            var gameCube = _gameCubePool.Get();
            gameCube.IsRelease = false;
            gameCube.Release += ReleaseGameCube;
            return gameCube;
        }

        public void ReleaseAllGameCubes()
        {
            foreach (var gameCube in _allGameCubes)
            {
                if (!gameCube.IsRelease)
                {
                    gameCube.MatchRelease();
                }
            }
        }

        private void CreateGameCubePool()
        {
            _gameCubePool = new ObjectPool<GameCube>(
                OnCreateGameCube,
                OnGetGameCube,
                OnReleaseGameCube,
                OnDestroyGameCube,
                true,
                20,
                100);
        }

        private void ReleaseGameCube(GameCube gameCube)
        {
            gameCube.Release -= ReleaseGameCube;
            gameCube.IsRelease = true;
            _gameCubePool.Release(gameCube);
        }

        private GameCube OnCreateGameCube()
        {
            GameCube gameCube = Instantiate(_gameCubePrefab);
            _scoreCounter.AddCubeForTracking(gameCube);
            _gameSounds.AddCubeForTracking(gameCube);
            _gameVibration.AddCubeForTracking(gameCube);
            _allGameCubes.Add(gameCube);
            return gameCube;
        }

        private void OnGetGameCube(GameCube gameCube)
        {
            gameCube.gameObject.SetActive(true);
        }
        
        private void OnReleaseGameCube(GameCube gameCube)
        {
            gameCube.gameObject.SetActive(false);
        }

        private void OnDestroyGameCube(GameCube gameCube)
        {
            _scoreCounter.RemoveCubeForTracking(gameCube);
            _gameSounds.RemoveCubeForTracking(gameCube);
            _gameVibration.RemoveCubeForTracking(gameCube);
            Destroy(gameCube.gameObject);
        }
    }
}