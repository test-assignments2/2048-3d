using UnityEngine;

namespace DefaultNamespace
{
    public class StartPosition : MonoBehaviour
    {
        [SerializeField] private Transform _leftPosition;
        [SerializeField] private Transform _rightPosition;

        public float LeftX => _leftPosition.position.x;
        public float RightX => _rightPosition.position.x;
        public float Y => _leftPosition.position.y;
        public float Z => _leftPosition.position.z;
    }
}