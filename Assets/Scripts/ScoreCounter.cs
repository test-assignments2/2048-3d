using UI;
using UnityEngine;

namespace DefaultNamespace
{
    public class ScoreCounter
    {
        private ScoreView _scoreView;
        private int _score;
        
        public ScoreCounter(ScoreView scoreView)
        {
            _scoreView = scoreView;
        }

        public void AddCubeForTracking(GameCube gameCube)
        {
            gameCube.LevelIsUp += UpdateScore;
        }

        public void RemoveCubeForTracking(GameCube gameCube)
        {
            gameCube.LevelIsUp -= UpdateScore;
        }

        public void Cleare()
        {
            _score = 0;
            _scoreView.SetScore(_score);
        }

        private void UpdateScore(int level)
        {
            _score += (int)Mathf.Pow(2,level);
            _scoreView.SetScore(_score);
        }
    }
}