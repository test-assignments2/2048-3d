using DefaultNamespace;
using Input;
using UnityEngine;

public class EntryPoint : MonoBehaviour
{
    [SerializeField] private GameManager _gameManager;
    
    [Header("Input")]
    [SerializeField] private GameplayInputManager _gameplayInputManager;
    [SerializeField] private CubeLauncherMoveHandler _cubeLauncherMoveHandler;
    [SerializeField] private GameCubeRunHandler _gameCubeRunHandler;
    [SerializeField] private RectTransform _moveInputZone;



    private void Awake()
    {
        InitializeInput();
        _gameManager.Init(_gameplayInputManager);
    }

    private void InitializeInput()
    {
        _gameplayInputManager.Init(_moveInputZone);
        _cubeLauncherMoveHandler.Init(_gameplayInputManager);
        _gameCubeRunHandler.Init(_gameplayInputManager);
    }
}