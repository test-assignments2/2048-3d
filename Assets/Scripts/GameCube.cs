using System;
using DefaultNamespace.Data;
using TMPro;
using UnityEngine;

namespace DefaultNamespace
{
    public class GameCube : MonoBehaviour
    {
        [SerializeField] private TextMeshPro[] _texts;
        [SerializeField] private MeshRenderer _meshRenderer;
        [SerializeField] private Rigidbody _rigidbody;
        [SerializeField] private float _matchUpForce = 4f;

        public event Action<GameCube> Release;
        public event Action<int> LevelIsUp;
        public event Action RunCube;
        public bool IsRun { get; set; }
        public bool IsCharge { get; set; }
        public bool IsRelease { get; set; }
        public int Level { get; private set; }
        public int SerialNumber { get; private set; }
        public float RunTime { get; private set; }

        private GameCubeColors _colors;

        public void Init(int level, GameCubeColors colors, int serialNumber)
        {
            _colors = colors;
            SetLevel(level);
            SerialNumber = serialNumber;
        }

        public void Run(Vector3 force)
        {
            RunTime = Time.time;
            _rigidbody.AddForce(force, ForceMode.Impulse);
            IsCharge = false;
            RunCube?.Invoke();
        }

        public void LevelUp()
        {
            int nextLevel = Level + 1;
            SetLevel(nextLevel);
            _rigidbody.AddForce(Vector3.up * _matchUpForce, ForceMode.Impulse);
            LevelIsUp?.Invoke(nextLevel);
        }

        public void MatchRelease()
        {
            _rigidbody.velocity = Vector3.zero;
            _rigidbody.angularVelocity = Vector3.zero;
            transform.rotation = Quaternion.identity;
            IsRun = false;
            IsCharge = false;
            Release?.Invoke(this);
        }

        private void SetLevel(int level)
        {
            Level = level;
            var number = Mathf.Pow(2, level);
            var line = number.ToString();
            foreach (var text in _texts)
            {
                text.text = line;
            }

            _meshRenderer.material = _colors.GetMaterial(level);
        }

        private void OnCollisionEnter(Collision other)
        {
            if (other.gameObject.TryGetComponent(out GameCube otherCube))
            {
                if (otherCube.Level == Level && otherCube.SerialNumber < SerialNumber)
                {
                    LevelUp();
                    otherCube.MatchRelease();
                }
            }
        }
    }
}