using System;
using UnityEngine;

namespace DefaultNamespace
{
    public class StartZone : MonoBehaviour
    {
        public event Action CubeIsRun;
        public event Action GameOver;

        private const float _cubeRanDelay = 1f;
        
        private void OnTriggerExit(Collider other)
        {
            if (other.gameObject.TryGetComponent(out GameCube gameCube))
            {
                gameCube.IsRun = true;
                CubeIsRun?.Invoke();
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.TryGetComponent(out GameCube gameCube))
            {
                if (gameCube.IsRun)
                {
                    GameOver?.Invoke();
                }
            }
        }

        private void OnTriggerStay(Collider other)
        {
            if (other.gameObject.TryGetComponent(out GameCube gameCube))
            {
                if (gameCube.IsCharge)
                {
                    return;
                }

                if (gameCube.RunTime + _cubeRanDelay < Time.time)
                {
                    GameOver?.Invoke();
                }
            }
        }
    }
}