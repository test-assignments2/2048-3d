using DefaultNamespace.Data;
using Input;
using UI;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace UI
{
}

namespace DefaultNamespace
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] private CubeLauncher _cubeLauncher;
        [SerializeField] private StartZone _startZone;
        [SerializeField] private GameCubeColors _colors;
        [SerializeField] private GameSounds _gameSounds;

        [SerializeField] private Button _pauseButton;
        [SerializeField] private ScoreView _scoreView;
        [SerializeField] private GameOverPanel _gameOverPanel;
        [SerializeField] private PausePanel _pausePanel;

        private GameplayInputManager _inputManager;
        private int _minLevel = 1;
        private int _maxLevel = 7;
        private ScoreCounter _scoreCounter;
        private GameVibration _gameVibration;

        public void Init(GameplayInputManager inputManager)
        {
            _inputManager = inputManager;
            _scoreCounter = new ScoreCounter(_scoreView);
            _gameVibration = new GameVibration();
            _cubeLauncher.Init(_colors, _scoreCounter, _gameSounds, _gameVibration);
            _startZone.CubeIsRun += OnCubeIsRun;
            _startZone.GameOver += OnGameOver;
            
            _pauseButton.onClick.AddListener(OnPauseButtonClick);
            _gameOverPanel.Init(this);
            _pausePanel.Init(this, _gameSounds, _gameVibration);
            StartGame();
        }

        public void StartGame()
        {
            _cubeLauncher.ClearGameField();
            _scoreCounter.Cleare();
            DisablePause();
            AddNewCube();
        }

        public void EnablePause()
        {
            Time.timeScale = 0;
            _inputManager.Disable();
        }

        public void DisablePause()
        {
            Time.timeScale = 1;
            _inputManager.Enable();
        }

        private void OnPauseButtonClick()
        {
            _pausePanel.gameObject.SetActive(true);
            EnablePause();
        }

        private void OnGameOver()
        {
            _gameOverPanel.gameObject.SetActive(true);
            EnablePause();
        }

        private void OnCubeIsRun()
        {
            AddNewCube();
        }

        private void AddNewCube()
        {
            _cubeLauncher.ChargeCube(Random.Range(_minLevel, _maxLevel));
        }
    }
}