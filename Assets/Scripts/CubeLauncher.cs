using DefaultNamespace.Data;
using UnityEngine;

namespace DefaultNamespace
{
    public class CubeLauncher : MonoBehaviour
    {
        [SerializeField] private CubeSpawner _cubeSpawner;
        [SerializeField] private MeshRenderer _directionLine;
        [SerializeField] private float _force = 20f;

        private GameCubeColors _colors;
        private GameCube _currentGameCube;
        private int _serialNumberCounter;

        public void Init(GameCubeColors colors, ScoreCounter scoreCounter, GameSounds gameSounds, GameVibration gameVibration)
        {
            _colors = colors;
            _cubeSpawner.Init(scoreCounter, gameSounds, gameVibration);
        }

        public void ChargeCube(int level)
        {
            _directionLine.enabled = true;
            _currentGameCube = _cubeSpawner.CreateGameCube();
            _currentGameCube.transform.parent = gameObject.transform;
            _currentGameCube.transform.localPosition = Vector3.zero;
            _currentGameCube.Init(level, _colors, _serialNumberCounter++);
            _currentGameCube.IsCharge = true;
        }

        public void RunCube()
        {
            if (_currentGameCube != null)
            {
                _currentGameCube.transform.parent = null;
                _currentGameCube.Run(Vector3.forward * _force);
                _currentGameCube = null;
                _directionLine.enabled = false;
            }
        }

        public void ClearGameField()
        {
            _cubeSpawner.ReleaseAllGameCubes();
        }
    }
}