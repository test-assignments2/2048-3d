using UnityEngine;

namespace DefaultNamespace
{
    public class GameSounds : MonoBehaviour
    {
        [SerializeField] private AudioSource _runCube;
        [SerializeField] private AudioSource _levelUpCube;
        [SerializeField] private AudioSource _musicSound;

        private bool _sounds = true;
        private bool _music;
        public bool Sounds { get => _sounds; set => _sounds = value; }
        public bool Music
        {
            get => _music;
            set
            {
                _music = value;
                UpdateMusicStatus();
            }
        }

        private void UpdateMusicStatus()
        {
            if (_music)
            {
                _musicSound.Play();
            }
            else
            {
                _musicSound.Stop();
            }
        }
        
        public void AddCubeForTracking(GameCube gameCube)
        {
            gameCube.LevelIsUp += PlayLevelUpSound;
            gameCube.RunCube += PlayRunSound;
        }

        public void RemoveCubeForTracking(GameCube gameCube)
        {
            gameCube.LevelIsUp -= PlayLevelUpSound;
            gameCube.RunCube -= PlayRunSound;
        }

        private void PlayRunSound()
        {
            if (Sounds)
            {
                _runCube.Play();
            }
        }

        private void PlayLevelUpSound(int level)
        {
            if (Sounds)
            {
                _levelUpCube.Play();
            }
        }
    }
}